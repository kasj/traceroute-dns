#include <arpa/inet.h>
#include <netinet/ip_icmp.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>

#define POLL_TIMEOUT_MS 1000

enum mode_t
{
	CLIENT,
	SERVER
};

struct dns_msg_header
{
	uint16_t id;         /* Identifier */

	uint8_t rd : 1;      /* Recursion Desired */
	uint8_t tc : 1;      /* Truncation */
	uint8_t aa : 1;      /* Authoritative Answer */
	uint8_t opcode : 4;  /* Operation Code */
	uint8_t qr : 1;      /* Query/Response */

	uint8_t rcode : 4;   /* Response Code */
	uint8_t cd : 1;      /* Checking Disabled */
	uint8_t ad : 1;      /* Authenticated Data */
	uint8_t z : 1;       /* Reserved */
	uint8_t ra : 1;      /* Recursion Available */

	uint16_t qdcount;    /* Question Count */
	uint16_t ancount;    /* Answer Record Count */
	uint16_t nscount;    /* Authority Record Count */
	uint16_t arcount;    /* Additional Record Count */
};

struct net_cfg
{
	int recv_icmp;
	int recv_udp;
	int send_udp;
	int port;
	char *ip;
	struct sockaddr_in local;
	struct sockaddr_in remote;
};

struct trace_cfg
{
	enum mode_t mode;
	int max_ttl;
	int wait_sec;
};

void usage(char *name);
void create_sockets(struct net_cfg *, struct trace_cfg *);
int create_dns_msg(uint8_t *, struct trace_cfg *);
int traceroute(struct net_cfg *, struct trace_cfg *, uint8_t *, int);

int main(int argc, char *argv[])
{
	int msg_length, opt;
	uint8_t msg[80];
	struct net_cfg net;
	struct trace_cfg trace;

	net.port = 53;
	trace.mode = CLIENT;
	trace.max_ttl = 30;
	trace.wait_sec = 3;

	while ((opt = getopt(argc, argv, "csp:t:w:h")) != -1) {
		switch (opt) {
		case 'c':
			trace.mode = CLIENT;
			break;
		case 's':
			trace.mode = SERVER;
			break;
		case 'p':
			net.port = atoi(optarg);
			break;
		case 't':
			trace.max_ttl = atoi(optarg);
			break;
		case 'w':
			trace.wait_sec = atoi(optarg);
			break;
		case 'h':
		default:
			usage(argv[0]);
			break;
		}
	}

	if (argc - optind != 1) {
		usage(argv[0]);
	} else {
		net.ip = argv[optind];
	}

	/* Initialize random number generator */
	srand(time(NULL));

	create_sockets(&net, &trace);
	msg_length = create_dns_msg(msg, &trace);
	traceroute(&net, &trace, msg, msg_length);

	return EXIT_SUCCESS;
}

void usage(char *name)
{
	fprintf(stderr, "Usage: %s [-cs] [-p port] [-t ttl] [-w sec] destination\n\n", name);
	fprintf(stderr, "Trace the route to destination using DNS messages\n\n");
	fprintf(stderr, "  -c\tclient mode (default)\n");
	fprintf(stderr, "  -s\tserver mode\n");
	fprintf(stderr, "  -p\tdestination port in client mode and source port in server mode "
                    "(default 53)\n");
	fprintf(stderr, "  -t\tmax TTL (default 30)\n");
	fprintf(stderr, "  -w\ttime in seconds to wait for a response (default 3)\n\n");

	exit(EXIT_FAILURE);
}

void create_sockets(struct net_cfg *net, struct trace_cfg *trace)
{
	int local_port, remote_port;

	if (trace->mode == CLIENT) {
		local_port = 0;
		remote_port = net->port;
	} else {
		local_port = net->port;
		remote_port = (uint16_t) rand();
	}

	memset(&(net->local), 0, sizeof(net->local));
	net->local.sin_family = AF_INET;
	net->local.sin_port = htons(local_port);

	memset(&(net->remote), 0, sizeof(net->remote));
	net->remote.sin_family = AF_INET;
	net->remote.sin_port = htons(remote_port);
	inet_pton(AF_INET, net->ip, &(net->remote.sin_addr));


	if ((net->send_udp = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		perror("socket(): send_udp");
		exit(EXIT_FAILURE);
	}

	if ((net->recv_icmp = socket(PF_INET, SOCK_RAW, IPPROTO_ICMP)) == -1) {
		perror("socket(): recv_udp");
		exit(EXIT_FAILURE);
	}

	if ((net->recv_udp = socket(PF_INET, SOCK_RAW, IPPROTO_UDP)) == -1) {
		perror("socket(): recv_udp");
	}

	if (trace->mode == SERVER) {
		if ((bind(net->send_udp, (struct sockaddr *) &(net->local),
            sizeof(net->local))) == -1) {

			perror("bind(): send_udp");
			exit(EXIT_FAILURE);
		}
	}
}

int create_dns_msg(uint8_t *msg_buffer, struct trace_cfg *trace)
{
	int offset;
	struct dns_msg_header header;

	const uint8_t dns_query[] = { 0x09, 0x61, 0x6c, 0x74, 0x61, 0x76, 0x69, 0x73,
                                  0x74, 0x61, 0x03, 0x63, 0x6f, 0x6d, 0x00, 0x00,
                                  0x01, 0x00, 0x01 };

	const uint8_t dns_response[] = { 0x09, 0x61, 0x6c, 0x74, 0x61, 0x76, 0x69, 0x73,
                                     0x74, 0x61, 0x03, 0x63, 0x6f, 0x6d, 0x00, 0x00,
                                     0x01, 0x00, 0x01, 0xc0, 0x0c, 0x00, 0x01, 0x00,
                                     0x01, 0x00, 0x00, 0x00, 0xf3, 0x00, 0x04, 0xd4,
                                     0x52, 0x66, 0x18, 0xc0, 0x0c, 0x00, 0x01, 0x00,
                                     0x01, 0x00, 0x00, 0x00, 0xf3, 0x00, 0x04, 0x4a,
                                     0x06, 0x32, 0x18, 0xc0, 0x0c, 0x00, 0x01, 0x00,
                                     0x01, 0x00, 0x00, 0x00, 0xf3, 0x00, 0x04, 0xd4,
                                     0xee, 0xb8, 0x18 };

	memset(&header, 0, sizeof(header));

	if (trace->mode == CLIENT) {
		header.id = (uint16_t) htons(rand());
		header.rd = 1;
		header.qdcount = htons(1);

		offset = 0;
		memcpy(msg_buffer + offset, &header, sizeof(header));
		offset += sizeof(header);
		memcpy(msg_buffer + offset, dns_query, sizeof(dns_query));
		offset += sizeof(dns_query);
	} else {
		header.id = (uint16_t) htons(rand());
		header.qr = 1;
		header.rd = 1;
		header.ra = 1;
		header.qdcount = htons(1);
		header.ancount = htons(3);

		offset = 0;
		memcpy(msg_buffer + offset, &header, sizeof(header));
		offset += sizeof(header);
		memcpy(msg_buffer + offset, dns_response, sizeof(dns_response));
		offset += sizeof(dns_response);
	}

	return offset;
}

int traceroute(struct net_cfg *net, struct trace_cfg *trace, uint8_t *msg, int msg_length)
{
	int i, ttl, icmp_type, stop_trace, timeouts;
	uint8_t recv_buffer[128], dst_ip[INET_ADDRSTRLEN], reply_ip[INET_ADDRSTRLEN];
	float result;
	struct iphdr *ip_header;
	struct icmphdr *icmp_header;
	struct timespec send_time, recv_time;
	struct pollfd sockfd[2];

	sockfd[0].fd = net->recv_icmp;
	sockfd[0].events = POLLIN;
	sockfd[1].fd = net->recv_udp;
	sockfd[1].events = POLLIN;

	inet_ntop(AF_INET, &(net->remote.sin_addr), dst_ip, INET_ADDRSTRLEN);

	stop_trace = timeouts = 0;

	for (ttl = 1; ttl <= trace->max_ttl && stop_trace == 0; ttl++) {
		if ((setsockopt(net->send_udp, IPPROTO_IP, IP_TTL, &ttl, sizeof(ttl))) == -1) {
			perror("setsockopt(): send_udp");
			exit(1);
		}

		if (sendto(net->send_udp, msg, msg_length, 0, (struct sockaddr *)
            &(net->remote), sizeof(net->remote)) == -1) {

			perror("sendto(): send_udp");
			exit(1);
		}

		if (timeouts == 0) {
			clock_gettime(CLOCK_MONOTONIC, &send_time);
		}

		if (poll(sockfd, 2, POLL_TIMEOUT_MS) <= 0) {
			if (timeouts == trace->wait_sec) {
				printf("%2d  ?\n", ttl);
				timeouts = 0;
				continue;
			}

			ttl--;
			timeouts++;
			continue;
		}

		clock_gettime(CLOCK_MONOTONIC, &recv_time);

		for (i = 0; i < 2; i++) {
			if (sockfd[i].fd == net->recv_icmp && sockfd[i].revents == POLLIN) {
				read(net->recv_icmp, recv_buffer, sizeof(recv_buffer));

				icmp_header = (struct icmphdr *) (recv_buffer + sizeof(struct iphdr));
				icmp_type = icmp_header->type;

				if (icmp_type == 3) {
					printf("%2d  ?\n", ttl);
					return EXIT_FAILURE;
				}

			} else if (sockfd[i].fd == net->recv_udp && sockfd[i].revents == POLLIN) {
				read(net->recv_udp, recv_buffer, sizeof(recv_buffer));
			}
		}

		ip_header = (struct iphdr *) recv_buffer;

		inet_ntop(AF_INET, &(ip_header->saddr), reply_ip, INET_ADDRSTRLEN);

		if (! strcmp(dst_ip, reply_ip)) {
			stop_trace = 1;
		}

		result = ((recv_time.tv_sec - send_time.tv_sec) * 1.0e3) +
                 ((recv_time.tv_nsec - send_time.tv_nsec) / 1.0e6);

		printf("%2d  %-17s%.3f ms\n", ttl, reply_ip, result);

		timeouts = 0;
	}
}
